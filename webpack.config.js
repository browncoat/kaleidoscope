'use strict';

var path = require('path');
var WebpackNotifierPlugin = require('webpack-notifier');

var babelOptions = {
  "presets": [
    "latest",
  ],
};

var notifierOptions = {
    excludeWarnings: true,
    alwaysNotify: true,
};

module.exports = {
  cache: true,
  entry: {
    main: './src/main.js',
  },
  output: {
    path: path.resolve(__dirname, './dist'),
    filename: '[name].js',
    chunkFilename: '[chunkhash].js'
  },
  module: {
    rules: [{
      test: /\.js$/,
      exclude: /node_modules/,
      use: [
        {
          loader: 'babel-loader',
          options: babelOptions
        }
      ]
    }]
  },
  plugins: [
      new WebpackNotifierPlugin(notifierOptions),
  ],
  resolve: {
    extensions: [
        '.js'
    ]
  },
};