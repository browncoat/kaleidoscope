import range from 'lodash/range'

// Kaleidoscope Class
class Kaleidoscope {
  static initClass() {
    this.prototype.HALF_PI = Math.PI / 2;
    this.prototype.TWO_PI = Math.PI * 2;
  }

  constructor(options = {}) {
    this.HALF_PI = Math.PI / 2;
    this.TWO_PI = Math.PI * 2;

    this.defaults = {
      offsetRotation: 0.0,
      offsetScale: 1.0,
      offsetX: 0.0,
      offsetY: 0.0,
      radius: 260,
      slices: 12,
      zoom: 1.0,
    }

    for (let key in this.defaults) {
      this[key] = this.defaults[key]
    }
    for (let key in options) {
      this[key] = options[key]
    }

    if (this.domElement == null) {
      this.domElement = document.createElement('canvas')
    }
    if (this.context == null) {
      this.context = this.domElement.getContext('2d')
    }
    if (this.image == null) {
      this.image = document.createElement('img')
    }
  }
    
  draw() {
    this.domElement.width = this.domElement.height = this.radius * 2;
    this.context.fillStyle = this.context.createPattern(this.image, 'repeat');

    let scale = this.zoom * ( this.radius / Math.min(this.image.width, this.image.height) );
    let step = this.TWO_PI / this.slices;
    let cx = this.image.width / 2;

    return range(this.slices).map((index) => (
      this.context.save(),
      this.context.translate(this.radius, this.radius),
      this.context.rotate(index * step),

      this.context.beginPath(),
      this.context.moveTo(-0.5, -0.5),
      this.context.arc(0, 0, this.radius, step * -0.51, step * 0.51),
      this.context.lineTo(0.5, 0.5),
      this.context.closePath(),

      this.context.rotate(this.HALF_PI),
      this.context.scale(scale, scale),
      this.context.scale([-1,1][index % 2], 1),
      this.context.translate(this.offsetX - cx, this.offsetY),
      this.context.rotate(this.offsetRotation),
      this.context.scale(this.offsetScale, this.offsetScale),

      this.context.fill(),
      this.context.restore()
    ));
  }
}

export default Kaleidoscope