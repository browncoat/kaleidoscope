import Kaleidoscope from './kaleidoscope'

// Init kaleidoscope
  
let image = new Image
image.onload = () => kaleidoscope.draw()
// image.src = 'http://cl.ly/image/1X3e0u1Q0M01/cm.jpg'
image.src = './dist/ric.jpg'

let kaleidoscope = new Kaleidoscope({
  image,
  slices: 26
})

kaleidoscope.domElement.style.position = 'absolute'
kaleidoscope.domElement.style.marginLeft = -kaleidoscope.radius + 'px'
kaleidoscope.domElement.style.marginTop = -kaleidoscope.radius + 'px'
kaleidoscope.domElement.style.left = '50%'
kaleidoscope.domElement.style.top = '50%'
document.body.appendChild(kaleidoscope.domElement)

// Mouse events

let tx = kaleidoscope.offsetX
let ty = kaleidoscope.offsetY
let tr = kaleidoscope.offsetRotation
  
let onMouseMoved = event => {
  let cx = window.innerWidth / 2
  let cy = window.innerHeight / 2

  let dx = event.pageX / window.innerWidth
  let dy = event.pageY / window.innerHeight

  let hx = dx - 0.5
  let hy = dy - 0.5

  tx = hx * kaleidoscope.radius * -2
  ty = hy * kaleidoscope.radius * 2

  return tr = Math.atan2(hy, hx)
}
window.addEventListener('mousemove', onMouseMoved, false)

let onClick = event => {
  options.active = !options.active
}
window.addEventListener('click', onClick, false)

// Init
  
let options = {
  active: true,
  ease: 0.005
}

let updateFunction = () => {
  if (options.active) {
    let delta = tr - kaleidoscope.offsetRotation
    let theta = Math.atan2( Math.sin( delta ), Math.cos( delta ) )

    kaleidoscope.offsetX += ( tx - kaleidoscope.offsetX ) * options.ease
    kaleidoscope.offsetY += ( ty - kaleidoscope.offsetY ) * options.ease
    kaleidoscope.offsetRotation += ( theta - kaleidoscope.offsetRotation ) * options.ease

    kaleidoscope.draw()
  }

  setTimeout(updateFunction, 1000/60)
}            
let update = updateFunction()